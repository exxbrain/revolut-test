package com.exxbrain.revoluttest.ui.main

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.exxbrain.revoluttest.data.remote.RatesDto
import com.exxbrain.revoluttest.data.remote.RemoteService
import com.exxbrain.revoluttest.viewmodel.asFlowable
import com.exxbrain.revoluttest.viewmodel.asObservable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainViewModel @Inject constructor(
        private val remoteService: RemoteService,
        private val context: Context

) : ViewModel(), Disposable {

    val current: CurrencyViewModel
    var currencies = MutableLiveData<List<CurrencyViewModel>>()

    private var lastSucceededRatesDto: RatesDto = RatesDto("EUR", hashMapOf())

    private lateinit var disposables: CompositeDisposable

    init {
        current = createCurrencyViewModel("EUR", 100.toBigDecimal())
        current.editable = true
    }

    fun beginUpdates() {
        disposables = CompositeDisposable()
        val downloadRates = Observable.create<RatesDto> { ratesEmitter ->
            val interval = Flowable.interval(0, 1, TimeUnit.SECONDS, Schedulers.io())
            val code = current.code.asFlowable()
            val disposable = Flowable.merge(interval, code)
                    .observeOn(Schedulers.io())
                    .onBackpressureDrop()
                    .doOnNext { downloadRates(ratesEmitter) }
                    .subscribe()
            ratesEmitter.setDisposable(disposable)
        }

        val createCurrencies = BiFunction { ratesDto: RatesDto, value: String  ->
            return@BiFunction ratesDto.rates.map {
                val value = if (!value.isEmpty()) value.toBigDecimal() else 0.toBigDecimal()
                createCurrencyViewModel(it.key, it.value.toBigDecimal() * value)
            }
        }
        val disposable = Observable
                .combineLatest(
                        downloadRates,
                        current.value.asObservable(),
                        createCurrencies
                )
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    currencies.value = it
                }
                .subscribe()

        disposables.add(disposable)
    }

    private fun downloadRates(ratesEmitter: ObservableEmitter<RatesDto>) {
        remoteService
                .getRates(current.code.get()!!)
                .doOnSuccess {
                    lastSucceededRatesDto = it
                    ratesEmitter?.onNext(it)
                }
                .onErrorReturn {
                    Log.e(MainViewModel::class.simpleName,
                            "Error while getting rates for '${current.code.get()}'. Returning last succeeded ones.", it)
                    lastSucceededRatesDto
                }
                .subscribe()
    }
    private fun createCurrencyViewModel(code: String, value: BigDecimal): CurrencyViewModel {
        val currencyObj = Currency.getInstance(code)
        val currency = CurrencyViewModel()
        with(currency) {
            this.code.set(code)
            this.value.set("%.${currencyObj.defaultFractionDigits}f".format(value))
            image.set(drawableResourceByName("flag_${code.toLowerCase()}"))
            name.set(currencyObj.displayName)
            symbol.set(currencyObj.symbol)
        }
        return currency
    }

    override fun onCleared() {
        super.onCleared()
        dispose()
    }

    override fun isDisposed(): Boolean {
        return disposables.isDisposed
    }

    override fun dispose() {
        disposables.dispose()
    }

    private fun drawableResourceByName(aString: String): Drawable {
        val packageName = context.packageName
        val resId = context.resources.getIdentifier(aString, "drawable", packageName)
        return ContextCompat.getDrawable(context, resId)!!
    }

    fun selectCurrency(currencyViewModel: CurrencyViewModel) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo == null) {
            Log.w(MainViewModel::class.simpleName,
                    "Unable to select currency. Network is unreachable.")
            return
        }
        with(currencyViewModel) {
            current.code.set(code.get())
            current.image.set(image.get())
            current.name.set(name.get())
            current.symbol.set(symbol.get())
        }
    }
}
