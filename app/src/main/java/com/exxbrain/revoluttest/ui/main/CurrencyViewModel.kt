package com.exxbrain.revoluttest.ui.main

import android.graphics.drawable.Drawable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField

data class CurrencyViewModel(
        @Bindable
        var editable: Boolean = false
): BaseObservable() {
    val value = ObservableField<String>()
    val code = ObservableField<String>()
    val name = ObservableField<String>()
    val symbol = ObservableField<String>()
    val image = ObservableField<Drawable>()
}
