package com.exxbrain.revoluttest.ui.main

import androidx.recyclerview.widget.RecyclerView
import com.exxbrain.revoluttest.databinding.CurrencyItemBinding
import kotlinx.android.synthetic.main.currency_item.view.*

class CurrencyViewHolder(private val binding: CurrencyItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(viewModel: CurrencyViewModel, mainViewModel: MainViewModel) {
        binding.viewModel = viewModel
        itemView.setOnClickListener { mainViewModel.selectCurrency(viewModel) }
    }

    fun updateJustValue(item: CurrencyViewModel?) {
        itemView.value.text = item?.value?.get()
    }
}
