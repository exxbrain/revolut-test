package com.exxbrain.revoluttest.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.exxbrain.revoluttest.R
import com.exxbrain.revoluttest.databinding.MainActivityBinding
import com.exxbrain.revoluttest.viewmodel.ViewModelFactory
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.main_activity.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
        val adapter = CurrencyAdapter(viewModel)
        viewModel.currencies.observe(this, Observer { adapter.submitList(it) })

        val binding: MainActivityBinding = DataBindingUtil.setContentView(
                this, R.layout.main_activity)
        binding.viewModel = viewModel

        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        viewModel.beginUpdates()
    }

    override fun onPause() {
        super.onPause()
        viewModel.dispose()
    }
}

