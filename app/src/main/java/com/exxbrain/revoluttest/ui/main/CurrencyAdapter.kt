package com.exxbrain.revoluttest.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.exxbrain.revoluttest.databinding.CurrencyItemBinding

class CurrencyAdapter(private val viewModel: MainViewModel)
    : ListAdapter<CurrencyViewModel, CurrencyViewHolder>(
        object : DiffUtil.ItemCallback<CurrencyViewModel>() {
            override fun areItemsTheSame(oldItem: CurrencyViewModel, newItem: CurrencyViewModel) =
                    oldItem.code.get() == newItem.code.get()

            override fun areContentsTheSame(
                    oldItem: CurrencyViewModel, newItem: CurrencyViewModel) = oldItem.value.get() == newItem.value.get()

            override fun getChangePayload(oldItem: CurrencyViewModel, newItem: CurrencyViewModel): Any? {
                return "valueChanged"
            }
        }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CurrencyItemBinding.inflate(inflater, parent, false)
        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bindTo(getItem(position), viewModel)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int, payloads: MutableList<Any>) {
        val item = getItem(position)
        if (!payloads.isEmpty()) {
            holder.updateJustValue(item)
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

}