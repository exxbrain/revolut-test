package com.exxbrain.revoluttest

import android.app.Application
import com.exxbrain.revoluttest.data.remote.RemoteServiceModule
import com.exxbrain.revoluttest.ui.main.MainActivityModule
import com.exxbrain.revoluttest.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [
    AppModule::class,
    AndroidSupportInjectionModule::class,
    MainActivityModule::class,
    RemoteServiceModule::class,
    ViewModelModule::class
])
@Singleton
interface AppComponent : AndroidInjector<App> {
    override fun inject(application: App)
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}