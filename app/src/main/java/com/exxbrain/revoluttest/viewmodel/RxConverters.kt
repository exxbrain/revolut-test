package com.exxbrain.revoluttest.viewmodel

import androidx.databinding.ObservableField
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable

fun <T> ObservableField<T>.asObservable(): Observable<T> {
    return Observable.create {
        it.onNext(get()!!)
        val callback = object : androidx.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: androidx.databinding.Observable?, propertyId: Int) {
                it.onNext(get()!!)
            }
        }
        addOnPropertyChangedCallback(callback)
        it.setCancellable {
            removeOnPropertyChangedCallback(callback)
        }
    }
}

fun <T> ObservableField<T>.asFlowable(): Flowable<T> {
    return Flowable.create ({
        it.onNext(get()!!)
        val callback = object : androidx.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: androidx.databinding.Observable?, propertyId: Int) {
                it.onNext(get()!!)
            }
        }
        addOnPropertyChangedCallback(callback)
        it.setCancellable {
            removeOnPropertyChangedCallback(callback)
        }
    }, BackpressureStrategy.LATEST)
}