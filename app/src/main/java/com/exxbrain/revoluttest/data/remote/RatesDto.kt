package com.exxbrain.revoluttest.data.remote

data class RatesDto(val base: String, val rates: Map<String, Float>)