package com.exxbrain.revoluttest.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService {
    @GET("latest")
    fun getRates(@Query("base") base: String): Single<RatesDto>
}